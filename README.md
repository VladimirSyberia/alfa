Test task-project for Alfa Bank.  
  
Where done tasks:  
1 main/java/parser run TaskWithNumberOrder.class  
2 test/java/web run YandexMarket.class  
3 test/java/web run AlfaBank.class  
  
------------------------------  
File for task #1 is in main/java/resource/numbers.txt  
Files created in task #3 are in main/java/resource/... 

Project have simple unit tests for util classes in test/java/unittest  

-------------------------------  
NOTE:  
project 100% work for Google Chrome and MacOS Hight Sierra/Windows 10.  
For run tests with other browser add new profile for your browser.(def. browser for project is Chrome)  
